﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace AliECSManagement
{
    public class MInstanceQueryParam
    {
        public Instances Instances { get; set; } 
        public int TotalCount { get; set; }
        public string RequestId { get; set; }
        public int PageSize { get; set; }
        public int PageNumber { get; set; }
    }
    public class Instances
    {
        public List<MInstance> Instance { get; set; }
    }
    public class MInstance
    {
        public string InstanceId { get; set; }
        public DateTime StartTime { get; set; }
        public string InstanceName { get; set; }
        public MPublicIpAddress PublicIpAddress { get; set; } = new MPublicIpAddress();
        public MEipAddress EipAddress { get; set; } = new MEipAddress();
        public string Status { get; set; }
        public string ExpiredTime { get; set; }
        public string InstanceType { get; set; }
        public string AliACC { get; set; }
        public string GetPublicIP()
        {
            return EipAddress.IpAddress == string.Empty?PublicIpAddress.ToString(): EipAddress.IpAddress;
        }
    }

    //-- Public Changed IP 
    public class MEipAddress 
    {
        public string IpAddress { get; set; }
    }
    public class MPublicIpAddress
    {
        public List<string> IpAddress { get; set; } = new List<string>();
        public override string ToString()
        {
            if (IpAddress.Count > 0)
                return IpAddress[0];
            else
                return string.Empty;
        }
    }
}
