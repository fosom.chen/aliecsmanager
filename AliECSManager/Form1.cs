﻿using Aliyun.Acs.Core;
using Aliyun.Acs.Core.Profile;
using Aliyun.Acs.Ecs.Model.V20140526;
using Aliyun.Acs.Core.Exceptions;
using Amazon.AutoScaling.Model;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using AliECSManagement;

namespace AliECSManager
{
    public partial class Form1 : Form
    {
        List<AliECSManagement.MInstance> gAllInstance = new List<AliECSManagement.MInstance>();
        private void tlog(string str)
        {
            toolStripStatusLabel1.Text = str;
        }


        #region Form OnEvent
        public Form1()
        {
            InitializeComponent();

            label1.Visible = false;
            richTextBox1.Visible = false;
            SetListViewInit(listView1);
            textBox1.Text = "输入IP、机器名模糊查询";
            toolStripStatusLabel1.Text = $"{DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")}";
            this.Text = $"[V3 Studio -- 阿里云机管理工具] {Assembly.GetEntryAssembly().GetName().Version.ToString()}";
        }

        private void textBox1_Enter(object sender, EventArgs e)
        {
            if(textBox1.Text== "输入IP、机器名模糊查询")
                textBox1.Text = string.Empty;
        }
        private void textBox1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Return)
                ShowInstances(listView1, textBox1.Text);
        }
        //refresh
        private void button1_Click(object sender, EventArgs e)
        {
            button1.Enabled = false;
            //textBox1.Text = string.Empty;
            ReloadData();
            ShowInstances(listView1);
            button1.Enabled = true;
        }
        //restart
        private void button2_Click(object sender, EventArgs e)
        {
            if (listView1.SelectedItems == null) return;
            button2.Enabled = false;
            Reboot(listView1.SelectedItems[0].Text, listView1.SelectedItems[0].SubItems[7].Text);
            button2.Enabled = true;
        }
        private void listView1_ColumnClick(object sender, ColumnClickEventArgs e)
        {
            System.Windows.Forms.ListView lv = sender as System.Windows.Forms.ListView;
            if ((lv.ListViewItemSorter as ListViewColumnSorter) == null)
                lv.ListViewItemSorter = new ListViewColumnSorter();
            ListViewColumnSorter temp = (lv.ListViewItemSorter as ListViewColumnSorter);
            temp.StringNumOrder = true;
            if (e.Column == (lv.ListViewItemSorter as ListViewColumnSorter).SortColumn)
            {
                if ((lv.ListViewItemSorter as ListViewColumnSorter).OrderType == SortOrder.Ascending)
                {
                    (lv.ListViewItemSorter as ListViewColumnSorter).OrderType = SortOrder.Descending;
                }
                else
                {
                    (lv.ListViewItemSorter as ListViewColumnSorter).OrderType = SortOrder.Ascending;
                }
            }
            else
            {
                (lv.ListViewItemSorter as ListViewColumnSorter).SortColumn = e.Column;
                (lv.ListViewItemSorter as ListViewColumnSorter).OrderType = SortOrder.Ascending;
            }
            ((ListView)sender).Sort();
        }
        private void panel1_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            richTextBox1.Visible = !richTextBox1.Visible;
        }
        #endregion


        #region Function
        public static void SetListViewInit(ListView ti)
        {
            ti.FullRowSelect = true;
            //ti.CheckBoxes = false;
            ti.GridLines = true;
            ti.MultiSelect = false;
            ti.BackColor = Color.LightYellow; //LightSteelBlue, YellowGreen LightSalmon, LightYellow,
            ti.View = View.Details;
            ti.Columns.Clear();
            ti.Items.Clear();

            ColumnHeader ch1 = new ColumnHeader();
            ch1.Text = "实例ID";
            ch1.Width = 168;
            ti.Columns.Add(ch1);
            ColumnHeader ch2 = new ColumnHeader();
            ch2.Text = "公网IP";
            ch2.Width = 117;
            ti.Columns.Add(ch2);
            ColumnHeader ch3 = new ColumnHeader();
            ch3.Text = "机器名";
            ch3.Width = 94;
            ti.Columns.Add(ch3);
            ColumnHeader ch4 = new ColumnHeader();
            ch4.Text = "开启时间";
            ch4.Width = 135;
            ti.Columns.Add(ch4);
            ColumnHeader ch5 = new ColumnHeader();
            ch5.Text = "状态";
            ch5.Width = 78;
            ti.Columns.Add(ch5);
            ColumnHeader ch6 = new ColumnHeader();
            ch6.Text = "到期时间";
            ch6.Width = 143;
            ti.Columns.Add(ch6);
            ColumnHeader ch7 = new ColumnHeader();
            ch7.Text = "实例类型";
            ch7.Width = 161;
            ti.Columns.Add(ch7);
            ColumnHeader ch8 = new ColumnHeader();
            ch8.Text = "账户";
            ch8.Width = 80;
            ti.Columns.Add(ch8);
        }

        private DefaultAcsClient GetClient(string str)
        {
            if(str== "ybcaiwu666")
            {
                IClientProfile profile = DefaultProfile.GetProfile("cn-hongkong", "LTAI4GAeDrNXnHNU3c1HBnxv", "j3tRqhUjwZFNXZFeMv65SWhTKGKLrl");
                DefaultAcsClient client = new DefaultAcsClient(profile);
                return client;
            }
            else //(str== "ybcaiwu888")
            {
                IClientProfile profile = DefaultProfile.GetProfile("cn-hongkong", "LTAI4FzoMhM8gXa6e6u4qWC2", "tu2hpOceHthKqvFmnQ5S5KsItVs5Uf");
                DefaultAcsClient client = new DefaultAcsClient(profile);
                return client;
            }
            //BW运维-bobby, [23.04.20 22:03]        阿里云账户: ybcaiwu888        AccessKey ID:         AccessKeySecret: tu2hpOceHthKqvFmnQ5S5KsItVs5Uf            
        }
        private void _reloadData(string aliAcc)
        {
            DefaultAcsClient client = GetClient(aliAcc);
            var request = new DescribeInstancesRequest();
            request.PageSize = 100; //阿里支持的最大值            
            try
            {
                var response = client.GetAcsResponse(request);
                var qureyResult = System.Text.Encoding.UTF8.GetString(response.HttpResponse.Content);
                richTextBox1.AppendText(qureyResult);
                var datas = JsonConvert.DeserializeObject<MInstanceQueryParam>(qureyResult);
                for (int i = 0; i < datas.Instances.Instance.Count; i++)
                    datas.Instances.Instance[i].AliACC = aliAcc;
                gAllInstance.AddRange(datas.Instances.Instance);
                while (datas.TotalCount > request.PageSize && datas.TotalCount > datas.PageSize * datas.PageNumber)
                {
                    request.PageNumber = datas.PageNumber + 1;
                    response = client.GetAcsResponse(request);
                    qureyResult = System.Text.Encoding.UTF8.GetString(response.HttpResponse.Content);
                    richTextBox1.AppendText(qureyResult);
                    datas = JsonConvert.DeserializeObject<MInstanceQueryParam>(qureyResult);
                    for (int i = 0; i < datas.Instances.Instance.Count; i++)
                        datas.Instances.Instance[i].AliACC = aliAcc;
                    gAllInstance.AddRange(datas.Instances.Instance);
                }                                
            }
            catch (ServerException ex)
            {
                tlog(ex.ToString());
            }
            catch (ClientException ex)
            {
                tlog(ex.ToString());
            }
        }
        private void ReloadData()
        {
            richTextBox1.Clear();
            gAllInstance.Clear();
            _reloadData("ybcaiwu666");
            _reloadData("ybcaiwu888");
            tlog($"执行结果：共有 {gAllInstance.Count} 台");
        }
        private void ShowInstances(ListView listView, string filter="")
        {
            listView.Items.Clear();
            label1.Visible = false;
            foreach (AliECSManagement.MInstance v in gAllInstance.OrderBy(key =>key.InstanceName))
            {
                if((v.InstanceName.Contains(filter))||(v.GetPublicIP().Contains(filter)))
                {
                    ListViewItem ti = new ListViewItem();
                    ti.Text = v.InstanceId;
                    ti.SubItems.Add(v.GetPublicIP().ToString());
                    ti.SubItems.Add(v.InstanceName);
                    ti.SubItems.Add(v.StartTime.ToString("yyyy-MM-dd HH:mm:ss"));
                    ti.SubItems.Add(v.Status);
                    ti.SubItems.Add(v.ExpiredTime);
                    ti.SubItems.Add(v.InstanceType);
                    ti.SubItems.Add(v.AliACC);
                    listView.Items.Add(ti);
                }                
            }
            if (listView.Items.Count == 0)
                label1.Visible = true;
        }
        private void Reboot(string instanceID, string aliAcc)
        {
            DefaultAcsClient client = GetClient(aliAcc);
            var request = new RebootInstanceRequest();
            request.InstanceId = instanceID;
            request.ForceStop = true;
            try
            {
                var response = client.GetAcsResponse(request);
                tlog($"执行成功, 请稍后点击刷新按钮, 查看状态");
            }
            catch (ServerException ex)
            {
                tlog(ex.ToString());
            }
            catch (ClientException ex)
            {
                tlog(ex.ToString());
            }
        }


        #endregion

        
    }

}
